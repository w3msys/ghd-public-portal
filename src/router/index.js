import Vue from 'vue'
import Router from 'vue-router'
import AppWelcome from '@/components/welcome'
import AppHotelHome from '@/components/hotel-home.vue'
import AppRoomType from '@/components/room-type.vue'
import AppFacilities from '@/components/facilities.vue'
import AppGallery from '@/components/gallery.vue'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: AppWelcome,
      meta: {title: 'Tell Us what you are looking for'}
    },
    {
      path: '/hotel/:hotel_id',
      name: 'hotel-home',
      component: AppHotelHome,
      meta: {title: 'Hotel Home'},
      children: [
        {
          name: 'room-type',
          path: 'room-type',
          component: AppRoomType
        },
        {
          name: 'facilities',
          path: 'facilities',
          component: AppFacilities
        },{
          name: 'gallery',
          path: 'gallery',
          component: AppGallery
        }
      ]
    }
  ]
})
