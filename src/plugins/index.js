import './../assets/scss/main.scss';
import GlobalComponents from './global-components';
import uiHelper from './ui-helpers'

export default {
    install (Vue) {
        Vue.use(GlobalComponents)
        Vue.use(uiHelper)
    }
}