export default {
    install (Vue) {
        Vue.uiHelper = {

            windowHeight (target) {
                let height = (window.innerHeight) + 'px'
                // console.log(height)
                $(target).css({
                    minHeight: window.innerHeight + 'px'
                })

                $('.content .footer').css({
                    width: window.innerWidth + 'px',
                    left: '-50px',
                    right: '0px'
                })
            }
        }

        Vue.prototype.$uiHelper = Vue.uiHelper
    }
}