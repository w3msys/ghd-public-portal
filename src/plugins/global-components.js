import AppHeader from './../components/header.vue'
import {AdvSearch, Facility, Gallery, RoomType} from './../components/icons'

export default {
    install (Vue) {
        Vue.component('app-header', AppHeader);
        Vue.component('app-icon-adv-search', AdvSearch);
        Vue.component('app-icon-gallery', Gallery)
        Vue.component('app-icon-facility', Facility)
        Vue.component('app-icon-room-type', RoomType)
    }
}