import advSearch from './adv-search.vue';
import facility from './facility.vue';
import gallery from './gallery.vue'
import roomType from './room-type.vue'


export const AdvSearch = advSearch
export const Facility = facility
export const Gallery = gallery
export const RoomType = roomType